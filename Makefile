build:
	docker-compose build --pull --no-cache
up:
	docker-compose up -d

test:
	docker-compose run --rm php74-service php bin/phpunit -v

load:
	docker-compose run --rm php74-service php bin/console hautelook:fixtures:load

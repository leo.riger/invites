var userEmail = $('#infoUser').attr('data-user-email');
var userId = $('#infoUser').attr('data-user-id');

var requestGetSent = $.ajax({
    url: "http://localhost:8080/api/invites?page=1&sender.email=" + userEmail,
    type: "GET",
});

requestGetSent.done(function(data) {
    var sentInvites = data['hydra:member'];

    if (sentInvites) {
        sentInvites.forEach(function (item, index){
            var id = item['@id'];

            function displayCancelButton() {
                if ('sent' === item.status) {
                    return "<button data-id='"+id+"' class='cancelButton'><i class=\"fa-solid fa-ban\"></i></button>";
                }
                return "";
            }

            $("#sentList ul").append("" +
                "<li class='invite-"+item.status+"'> to:" + item.receiver.email + "- status: " + item.status + "   " + displayCancelButton() +"</li>" +
                ""
            );
        })
    }

    $("button.cancelButton").click(function () {
        var dataId = $(this).attr("data-id");
        onCancel(dataId);
    })
});

var requestGetReceived = $.ajax({
    url: "http://localhost:8080/api/invites?page=1&receiver.email=" + userEmail,
    type: "GET",
});

requestGetReceived.done(function(data) {
    var receivedInvites = data['hydra:member'];

    if (receivedInvites) {
        receivedInvites.forEach(function (item, index){
            var id = item['@id'];

            var changeStatusButtons = function () {
                if ('sent' === item.status) {
                    return "<button data-id='"+id+"' class='acceptButton'><i class=\"fa-solid fa-check\"></i></button>" +
                        "<button data-id='"+id+"' class='refuseButton'><i class=\"fa-regular fa-circle-xmark\"></i></button>" ;
                }

                return "";
            };


            $("#receivedList ul").append(
                "<li class='invite-"+item.status+"'> from:" + item.sender.email + "- status: " + item.status + "   " + changeStatusButtons() +"</li>"
            );
        })
    }

    $("button.refuseButton").click(function (e) {
        var dataId = $(this).attr("data-id");
        onRefuse(dataId);
    })

    $("button.acceptButton").click(function () {
        var dataId = $(this).attr("data-id");
        onAccept(dataId);
    })
});

var onRefuse = function (dataId) {
    updateStatus(dataId, 'refused')
}

var onAccept = function (dataId) {
    updateStatus(dataId, 'accepted')
}

var onCancel = function (dataId) {
    updateStatus(dataId, 'canceled')
}

var updateStatus = function (dataId, newStatus) {
    $.ajax({
        url: "http://localhost:8080" + dataId,
        type: "PUT",
        beforeSend: function( xhr ) {
            xhr.setRequestHeader('Content-type', 'application/ld+json');
            xhr.setRequestHeader('accept', 'application/ld+json');
        },
        data: JSON.stringify({ status: newStatus }),
    });

    refreshPage();
}

var refreshPage = function () {
    location.reload();

    return false;
}

var requestGetUsers = $.ajax({
    url: "http://localhost:8080/api/users",
    type: "GET",
});

requestGetUsers.done(function(data) {
    var users = data['hydra:member'];

    if (users) {
        users.forEach(function (user, index){
            var receiverUri = user['@id'];

            $("#emailList select").append("<option value='"+receiverUri+"'>to: " + user.email + "</option>");
        })
    }

    $("#emailList button").click(function () {
        var receiverUri = $("#selectList").val();

        sendInvite(receiverUri);
    })
});

var sendInvite = function (receiverUri) {

    var senderUri = "/api/users/"+ userId;

    $.ajax({
        url: "http://localhost:8080/api/invite",
        type: "POST",
        beforeSend: function( xhr ) {
            xhr.setRequestHeader('Content-type', 'application/ld+json');
            xhr.setRequestHeader('accept', 'application/ld+json');
        },
        data: JSON.stringify({ sender: senderUri, receiver: receiverUri }),
    });

    refreshPage();
}

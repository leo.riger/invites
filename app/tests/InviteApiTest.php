<?php

namespace App\Tests;

use ApiPlatform\Core\Bridge\Symfony\Bundle\Test\ApiTestCase;
use Hautelook\AliceBundle\PhpUnit\ReloadDatabaseTrait;
use Symfony\Contracts\HttpClient\ResponseInterface;

class InviteApiTest extends ApiTestCase
{
    use ReloadDatabaseTrait;

    public function testGetInvite(): void
    {
        $client = self::createClient();

        $crawler = $client->request('GET', 'http://localhost:8080/api/invites?page=1');

        $content = \json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('@context', $content);
        $this->assertArrayHasKey('@id', $content);
        $this->assertArrayHasKey('@type', $content);
        $this->assertArrayHasKey('hydra:member', $content);
        $this->assertArrayHasKey('hydra:totalItems', $content);
    }

    public function testGetInviteBySender(): void
    {
        $client = self::createClient();

        $crawler = $client->request('GET', 'http://localhost:8080/api/invites?page=1&receiver.email=receiver@test.com');

        $content = \json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('hydra:totalItems', $content);
        $this->assertSame(2, $content['hydra:totalItems']);
    }

    public function testPostInvite(): void
    {
        $client = self::createClient();

        $sender = $this->getApiUser('sender@test.com');
        $senderId = json_decode($sender->getContent(), true)['hydra:member'][0]['@id'];

        $receiver = $this->getApiUser('receiver@test.com');
        $receiverId = json_decode($receiver->getContent(), true)['hydra:member'][0]['@id'];

        $crawler = $client->request(
            'POST',
            'http://localhost:8080/api/invite',
            [
                'json' => [
                    'sender' => $senderId,
                    'receiver' => $receiverId,
                    'status' => 'sent'
                ],
                'headers' => [
                    'accept' => 'application/ld+json',
                    'content-type' => 'application/ld+json',
                ]
            ]
        );

        $content = \json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('@context', $content);
        $this->assertArrayHasKey('@id', $content);
        $this->assertArrayHasKey('@type', $content);
        $this->assertArrayHasKey('sender', $content);
        $this->assertArrayHasKey('receiver', $content);
        $this->assertArrayHasKey('status', $content);
    }

    public function testCantPostInviteWithWrongStatus(): void
    {
        $client = self::createClient();

        $sender = $this->getApiUser('sender@test.com');
        $senderId = json_decode($sender->getContent(), true)['hydra:member'][0]['@id'];

        $receiver = $this->getApiUser('receiver@test.com');
        $receiverId = json_decode($receiver->getContent(), true)['hydra:member'][0]['@id'];

        try {
            $crawler = $client->request(
                'POST',
                'http://localhost:8080/api/invite',
                [
                    'json' => [
                        'sender' => $senderId,
                        'receiver' => $receiverId,
                        'status' => 'wrong_status'
                    ],
                    'headers' => [
                        'accept' => 'application/ld+json',
                        'content-type' => 'application/ld+json',
                    ]
                ]
            );

            $content = \json_decode($client->getResponse()->getContent(), true);
        } catch (\Throwable $exception) {
            $this->assertStringContainsString(
                'status: The value you selected is not a valid choice.',
                $exception->getMessage()
            );
        }
    }

    private function getApiUser(string $email): ResponseInterface
    {
        $client = self::createClient();

        return $client->request('GET', "http://localhost:8080/api/users?email=" . $email);
    }

    public function testCanUpdateInviteStatus(): void
    {
        $client = self::createClient();

        $crawler = $client->request(
            'PUT',
            'http://localhost:8080/api/invites/1',
            [
                'json' => [
                    'status' => 'canceled'
                ],
                'headers' => [
                    'accept' => 'application/ld+json',
                    'content-type' => 'application/ld+json',
                ]
            ]
        );

        $content = \json_decode($client->getResponse()->getContent(), true);

        $this->assertArrayHasKey('@context', $content);
        $this->assertArrayHasKey('@id', $content);
        $this->assertArrayHasKey('@type', $content);
        $this->assertArrayHasKey('sender', $content);
        $this->assertArrayHasKey('receiver', $content);
    }
}
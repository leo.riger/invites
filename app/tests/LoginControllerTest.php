<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class LoginControllerTest extends WebTestCase
{
    public function testILandOnLoginPage(): void
    {
        $client = self::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseRedirects($crawler->getUri() . 'login');
    }

    public function testLoginPageIsAccessible(): void
    {
        $client = self::createClient();
        $crawler = $client->request('GET', '/login');

        $this->assertResponseStatusCodeSame(Response::HTTP_OK);
    }

    public function testLoginPageShowsWelcomeMessage(): void
    {
        $client = self::createClient();
        $crawler = $client->request('GET', '/login');

        $this->assertSelectorTextContains('h2', 'Welcome to Invites');
    }

    public function testAdminCanLogin(): void
    {
        $client = self::createClient();
        $crawler = $client->request('GET', '/login');

        $buttonCrawlerNode = $crawler->selectButton('login');

        $form = $buttonCrawlerNode->form();

        $client->submit($form, [
            '_username' => 'admin@test.com',
            '_password' => 'test',
        ]);

        $client->followRedirect();

        $this->assertMatchesRegularExpression('/\/profile$/', $client->getRequest()->getUri());

        $this->assertSelectorTextContains('h3', 'Hello admin@test.com');
    }
}
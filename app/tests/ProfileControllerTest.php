<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProfileControllerTest extends WebTestCase
{
    public function testCantAccessProfileAsAnonymous()
    {
        $client = self::createClient();
        $crawler = $client->request('GET', '/profile');

        $this->assertResponseStatusCodeSame(302);
        $client->followRedirect();
        $this->assertResponseStatusCodeSame(200);

        $this->assertMatchesRegularExpression('/\/login$/', $client->getRequest()->getUri());
    }
}
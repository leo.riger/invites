<?php

namespace App\DataFixtures;

use App\Entity\Invite;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }

    public function load(ObjectManager $manager): void
    {
        // Admin
        $admin = (new User())
            ->setEmail('admin@test.com');

        $password = $this->passwordHasher->hashPassword(
            $admin,
            'test'
        );

        $admin->setPassword($password);

        $manager->persist($admin);

        // Basic users:

        $sender = (new User())
            ->setEmail('sender@test.com');
        $receiver = (new User())
            ->setEmail('receiver@test.com');

        $passwordSender = $this->passwordHasher->hashPassword(
            $sender,
            'test'
        );

        $passwordReceiver = $this->passwordHasher->hashPassword(
            $receiver,
            'test'
        );

        $sender->setPassword($passwordSender);
        $receiver->setPassword($passwordReceiver);

        $manager->persist($sender);
        $manager->persist($receiver);

        $manager->flush();


        $invite1 = (new Invite())
            ->setSender($sender)
            ->setReceiver($receiver)
        ;

        $invite2 = (new Invite())
            ->setSender($receiver)
            ->setReceiver($sender)
        ;

        $invite3 = (new Invite())
            ->setSender($admin)
            ->setReceiver($sender)
        ;

        $invite4 = (new Invite())
            ->setSender($admin)
            ->setReceiver($receiver)
        ;

        $manager->persist($invite1);
        $manager->persist($invite2);
        $manager->persist($invite3);
        $manager->persist($invite4);
        $manager->flush();
    }
}

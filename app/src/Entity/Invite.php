<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Repository\InviteRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass=InviteRepository::class)
 * @ApiResource(
 *      normalizationContext={"groups"={"get"}},
 *      collectionOperations={
 *          "get"={"normalizationContext"={"groups"={"get"}}},
 *          "post"={"method"="POST", "path"="/invite"}
 *      },
 *     itemOperations={
 *          "get"={"normalizationContext"={"groups"={"get"}}},
 *          "put",
 *     }
 * )
 * @ApiFilter(SearchFilter::class, properties={"receiver.email", "sender.email"})
 */
class Invite
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="sentInvites")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("get")
     */
    private $sender;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="receivedInvites")
     * @ORM\JoinColumn(nullable=false)
     * @Groups("get")
     */
    private $receiver;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Choice({"sent", "accepted", "refused", "canceled"})
     * @Groups("get")
     */
    private $status = 'sent';

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSender(): ?User
    {
        return $this->sender;
    }

    public function setSender(User $sender): self
    {
        $this->sender = $sender;

        return $this;
    }

    public function getReceiver(): ?User
    {
        return $this->receiver;
    }

    public function setReceiver(?User $receiver): self
    {
        $this->receiver = $receiver;

        return $this;
    }

    public function getStatus(): ?string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}

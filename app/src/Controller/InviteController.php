<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class InviteController extends AbstractController
{
    /**
     * @Route("/invite", name="invite")
     */
    public function listSent(): Response
    {
        return $this->render('invite/index.html.twig', [
            'user' => $this->getUser(),
        ]);
    }
}
